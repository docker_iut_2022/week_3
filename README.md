# week_3

week 3:  building an application using several containers running in parallel
- How to setup a network to share between containers 
- How to launch a multi-container based application
- Exercice: prepare your own multi-container application

## Docker network
### Concept
Docker is also isolating the container [network](https://docs.docker.com/network/bridge/) from the host. When a container is created, Docker also create a separate **network namespace**. By default, docker creates a linux bridge called **docker0** to the host. There is usually only one instance of the bridge per docker daemon, connecting the containers network namespaces to the host network. The actual connection between the bridge and the container networks is done by a **veth pair**, acting like a virtual Network Interface Card (NIC). There is one per **active** container. In most Linux containers, the default interface is **eth0**.

![alt text](pictures/veth.png)
*Image: dev.to/polarbit*
```
raph@raph-VirtualBox:~/iut/week_3$ docker run -it busybox:1.34.1 
/ # ifconfig
eth0      Link encap:Ethernet  HWaddr 02:42:AC:11:00:03  
          inet addr:172.17.0.3  Bcast:172.17.255.255  Mask:255.255.0.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:15 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:2203 (2.1 KiB)  TX bytes:0 (0.0 B)

lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)

/ # exit
raph@raph-VirtualBox:~/iut/week_3$ ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:24:83:72 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 83640sec preferred_lft 83640sec
    inet6 fe80::bf5c:61a6:2d95:aa0a/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:45:63:e2:b6 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:45ff:fe63:e2b6/64 scope link 
       valid_lft forever preferred_lft forever
5: vethedbec2b@if4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default 
    link/ether 22:93:7f:19:c7:15 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::2093:7fff:fe19:c715/64 scope link 
       valid_lft forever preferred_lft forever

```

You can see docker networks with the docker client command:

```
raph@raph-VirtualBox:~$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
4862843da9d5   bridge    bridge    local
b2c6603cea69   host      host      local
2a149c2c7847   none      null      local
```

The networks above are predefined and cannot be removed.
Networks are managed through 2 drivers: 
- **Bridge** (default): Bridges are isolated within a single instance of the docker daemon.
- **Overlay**: Using this driver, it is possible to link containers running on different instances of the docker daemon. It is used in clusters of Docker hosts, typically a Docker [swarm](https://docs.docker.com/engine/swarm/). Docker configuration disables the creation of this type of network outside a Docker swarm context.
### Create and use a custom docker network
A docker network can created with a single command:

```
raph@raph-VirtualBox:~$ docker network create -d bridge my-bridge
608965114eccc5cf93089d7a91a9a9c15856aa4a694af19c9948b3fdb66fa991
raph@raph-VirtualBox:~$ docker network ls
NETWORK ID     NAME        DRIVER    SCOPE
4862843da9d5   bridge      bridge    local
b2c6603cea69   host        host      local
608965114ecc   my-bridge   bridge    local
2a149c2c7847   none        null      local
```

The -d option here is optional for bridge network, as it is the default driver.

You can try to create an overlay based network, but it is usually disabled:

```
raph@raph-VirtualBox:~$ docker network create -d overlay my-overlay
Error response from daemon: This node is not a swarm manager. Use "docker swarm init" or "docker swarm join" to connect this node to swarm and try again.
```

Docker always create non-overlapping networks for you, but you can customize these new networks on creation:

```
docker network create --subnet=192.168.0.0/16 custom-network
```

You can provide the network on container creation:

```
docker run -it --network=custom-network busybox:1.34.1
```

Or you can attach it to an existing container:

```
docker network connect <bridge name> <container name>
```

You can even share a network stack between two containers:

```
docker run -it --network container:<name|id> busybox:1.34.1
```

You can **force a container IP** (assuming you matched the subnet):

```
docker run -it --network=custom-network --ip 192.168.0.12 busybox:1.34.1
```

Finally, you can remove a docker network with the `docker network rm` command.


## How to launch a multi-container based application

Applications based on containers are mixing volumes and bridges to send and receive information internally. Setting up such an application requires the following steps:
- **Split your app into services**. Example: a simple client-server web application can be made of three components: frontend, backend, and a database.
- Pull or build **images**
- Create the Docker **volumes and networks**.
- **Launch containers with the right configuration**: ports to open, runtime configuration for each container. A container typically handles one service in the application.
- **Monitor the docker container status**. You can monitor all the containers in real-time with the `docker stats --all` command:

```
CONTAINER ID   NAME              CPU %     MEM USAGE / LIMIT   MEM %     NET I/O   BLOCK I/O   PIDS
7dff7f1876ad   test              0.00%     556KiB / 7.772GiB   0.01%     2.94kB / 0B   0B / 0B     1
2143d19880f3   funny_shockley    0.00%     0B / 0B             0.00%     0B / 0B   0B / 0B     0
c1a0aee24196   cool_sutherland   0.00%     0B / 0B             0.00%     0B / 0B   0B / 0B     0
18e09dd4edbe   quirky_kirch      0.00%     0B / 0B             0.00%     0B / 0B   0B / 0B     0
24947bf06d1c   keen_ellis        0.00%     0B / 0B             0.00%     0B / 0B   0B / 0B     0
d1bd27310675   serene_wozniak    0.00%     0B / 0B             0.00%     0B / 0B   0B / 0B     0
b971dd8bc5c5   hungry_noyce      0.00%     0B / 0B             0.00%     0B / 0B   0B / 0B     0
54ae65de4746   charming_mendel   0.00%     0B / 0B             0.00%     0B / 0B   0B / 0B     0

```

You can **set the policy on status change**. This is usually done at the container creation. 

![alt text](pictures/restart_policy.png)
*Image: docs.docker.com* 
  
For example, this line creates a private docker registry and expose its port 5000 to the 1234 port of the host interface. 
Its **restart policy** is to relaunch itself automatically, if for any reason the container would stop:

`docker run -d -p 5000:1234 --restart=always --name my-registry registry:2`

## Run your own Wordpress

The following lines will allow you to run a [Wordpress](https://wordpress.com/) web application in just a few commands, using docker:

First, create the volume used for persistent data storage:

```
docker volume create db_data
```

Then, run the container with the database service:

```
docker run --name=db -d -v db_data:/var/lib/mysql --restart=always -e MYSQL_ROOT_PASSWORD=somewordpress -e MYSQL_DATABASE=wordpress -e MYSQL_USER=wordpress -e MYSQL_PASSWORD=wordpress mysql:5.7
```

You can now launch the container with the wordpress front-end:

```
docker run -d --name=wp -p 8000:80 --restart=always -e WORDPRESS_DB_HOST=db:3306 -e WORDPRESS_DB_USER=wordpress -e WORDPRESS_DB_PASSWORD=wordpress wordpress:latest
```

The final step is to connect the two containers networks:

```
docker network create my_wordpress_default
docker network connect my_wordpress_default db
docker network connect my_wordpress_default wp
```

The application is now available in your web browser, at [localhost:8000](localhost:8000).

![alt text](pictures/home_wordpress.png)

![alt text](pictures/home_wordpress_2.png)

## Exercice: set the communication parameters between containers

The goal of this exercice is to **instantiate containers of the factory image**, located in the *factory/* folder, and **maximize the production of goods in a limited period of time, 60s**.

- All containers from this image will be called factories in this exercice.
- All factories share a volume called **factory**, which must be mounted at */factory*. 
- A factory has a worker inside, trying to **produce goods**.
- If there are **more than 5 containers at the same time, the new workers give up and do nothing**. The container is immediately destroyed. You can checks who is working by looking inside */factory/locker_room*.
- The worker waits for raw materials to come into the factory **for 10 seconds**. 
	- If **raw materials come in through a socket**, the **production is 5 times** the raw material input units.
	- Otherwise, the worker will **produce a single unit of goods**.
- There are environment vars called **INPUT_PORT**, **OUTPUT_PORT** and **OUTPUT_IP**. If not set, the goods are stored in the factory **warehouse**, located in */factory/warehouse*. The warehouse is the final destination of goods and nothing inside it can be reused by factories.
- If the **OUTPUT_IP** is set, the worker will send all the produced goods to the given destination, to be used as **raw input in another factory**. If the **destination does not exist, all the goods will be lost**! 
- INPUT and OUTPUT PORTS are randomized values between 2000 and 3000 when not set, so even with the right IP most goods are lost by default.

You must set the docker containers properly and as fast as possible over a period of 60s. The goods in the warehouse are timestamped to compute the total sum on a given time period.

In order to do this, **you must use the --env, --network, --ip, -v options with docker run.** The option list is available in the official Docker documentation, and the relevant sections of all the previous lessons.

You can start by calling the containers by hand, but **you will need a shell script to automate** this and increase the factories output.


## Authors
Raphaël Bouterige
