import socket
import sys

if len(sys.argv) == 4:
    ip = sys.argv[1]
    port = int(sys.argv[2])
    wait_s = int(sys.argv[3])
else:
    ip = "localhost"
    port = 5000
    wait_s = 10

# Create a UDP socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.settimeout(wait_s)
# Bind the socket to the port
server_address = (ip, port)
s.bind(server_address)

data = []
try:
    data, address = s.recvfrom(8)
    with open('/tmp/goods', 'w') as f:
        f.write(str(int.from_bytes(data, byteorder='little')))
except socket.timeout as e:
    pass
