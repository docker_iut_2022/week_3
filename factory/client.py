import socket
import sys

if len(sys.argv) == 4:
    ip = sys.argv[1]
    port = int(sys.argv[2])
    goods = int(sys.argv[3]).to_bytes(8, byteorder='little')
else:
    ip = "127.0.0.1"
    port = 5000
    goods = int(1).to_bytes(8, byteorder='little')

# Create socket for server
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
s.sendto(goods, (ip, port))
s.close()
